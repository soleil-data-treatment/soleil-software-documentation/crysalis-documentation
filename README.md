# Aide et ressources de Crysalis pour Synchrotron SOLEIL

[<img src="https://www.rigaku.com/sites/default/files/Rigaku%20rgb_logo_2018.12.10.png" width="400"/>](https://www.rigaku.com/products/smc/crysalis)

## Résumé
- Réduction de données de diffraction. Reconstruction d'image. Permet l'analyse 
- Privé

## Sources
- Code source: Non, pas de distribution
- Documentation officielle: https://www.rigaku.com/products/smc/crysalis

## Navigation rapide
| Fichiers téléchargeables | Ressources annexes | Page pan-data |
| ------ | ------ | ------ |
| [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/crysalis-documentation/-/tree/master/documents) | [Support officiel](https://www.rigaku.com/request-product-info?prodline=2&product=8) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/97/crysalis-pro) |
| [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/crysalis-documentation/-/tree/master/dataset) |   |   |

## Installation
- Systèmes d'exploitation supportés: Windows
- Installation: Facile (tout se passe bien)

## Format de données
- en entrée: Images propriétaires issues de leurs propres détecteurs ou autres formats (nombre restreint) (ex: esperanto)
- en sortie: Images reconstruites (.jpeg) analyse d'images (.txt)
- sur un disque dur, sur la Ruche, sur la Ruche locale
